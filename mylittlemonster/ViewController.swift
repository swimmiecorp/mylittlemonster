//
//  ViewController.swift
//  mylittlemonster
//
//  Created by Wim van Deursen on 23-12-15.
//  Copyright © 2015 Wim van Deursen. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var monsterImg: UIImageView!
    
    
    override func viewDidLoad( ) {
        super.viewDidLoad( )
        
        var imgArray = [UIImage] ( )
        for var x = 1; x <= 4; x++ {
            let img = UIImage(named: "idle\(x).png")
            imgArray.append(img!)
        }
        
        monsterImg.animationImages = imgArray
        monsterImg.animationDuration = 1
        monsterImg.animationRepeatCount = 0
        monsterImg.startAnimating()
        
    }



}

